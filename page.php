<?php
get_header();

//header
$query = new WP_Query([
    'post_type' => 'page'
]);

?>
<nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: <?php echo get_theme_mod('navbar_color') ?>!important">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">

            <ul class="navbar-nav">

                <?php
                while ($query->have_posts()) : $query->the_post();

                ?>

                    <li class="nav-item">
                        <a class="nav-link active" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                    </li>

                <?php
                endwhile;
                ?>
            </ul>
        </div>
    </div>
</nav>
<?php


while (have_posts()) : the_post();

    the_title();

    the_content();


    comments_template();


endwhile;


get_footer();
