<?php 


while( have_posts() ):the_post();

    the_title();
    
    the_content();
    

    comments_template();


endwhile;
