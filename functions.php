<?php


add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

function my_theme_enqueue_styles()
{
    wp_enqueue_style(
        'child-style',
        get_stylesheet_uri(),
        [],
        wp_get_theme()->get('Version') // this only works if you have Version in the style header
    );
}

add_action('wp_enqueue_scripts', 'style_theme_enfant', 20);
function style_theme_enfant()
{
    wp_dequeue_style('twentytwentyone-style', get_stylesheet_uri());
    wp_enqueue_style('child-style', get_stylesheet_uri());
}

function load_bootstrap() {
    wp_enqueue_style('bootstrap5', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css');
    wp_enqueue_script( 'bootstrap5-scripts','https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js');
}
add_action( 'wp_enqueue_scripts', 'load_bootstrap' );


//Rajouter un menu au customizer

add_action('customize_register', 'customizer_header');

function customizer_header(WP_Customize_Manager $wp_customize) {
    $wp_customize->add_section('simplon_section', [
        'title' => 'Simplon Options',
        'description' => 'Simplon theme customizer options...',
        'priority' => '40'
    ]);

    $wp_customize->add_setting('navbar_color', [
        'default' => '#ffb8b8'
    ]);
    $wp_customize->get_setting('navbar_color')->transport = 'postMessage';

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'navbar_color', [
        'label' => 'Navbar color',
        'section' => 'simplon_section',
        'setting' => 'navbar_color'
    ]));

    $wp_customize->selective_refresh->add_partial( 'navbar_color', array(
        'selector' => '.navbar .navbar-brand',
        'render_callback' => 'simplon_navbar_color',
    ) );
}